# LibreWolf macOS

## Building

### Instructions
  
1.  `git clone --recursive https://gitlab.com/librewolf-community/browser/macos.git LibreWolf && ./LibreWolf/package.sh`
2.  Profit

    ```
    Downloading latest Firefox from mozilla...
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                   Dload  Upload   Total   Spent    Left  Speed
    100   120  100   120    0     0    185      0 --:--:-- --:--:-- --:--:--   185
    100 67.6M  100 67.6M    0     0  3481k      0  0:00:19  0:00:19 --:--:-- 3468k
    Mounting Firefox image
    Making Firefox great again
    Unmount Firefox image
    Cooking LibreWolf image
    All done.
    LibreFox img is in /Users/user/Download/LibreWolf
    ```
## License

Mozilla Public License Version 2.0. See `LICENSE.txt` for details.
