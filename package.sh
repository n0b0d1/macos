#!/bin/bash

# Usage: 
# ./package.sh [/path/to/Firefox.dmg]
# If path is not defined, latest release of Firefox will be downloaded and used
#
# LibreWolf.dmg will be created in ../LibreWolf dir

clear

url="https://download.mozilla.org/?product=firefox-latest-ssl&os=osx"
namefile="Firefox.dmg"
app="Firefox.app"

i=0
while [[ -d "Volumes/$namefile" && $i < 10 ]]; do
	namefile="$(basename "$namefile") $i.app"
	i=$((i+1))
done

if [[ $i == 10 ]]; then
	echo "You must eject Firefox dmg for run this script"
	exit
fi

repo="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

rm -rf "$repo"/../LibreWolf.dmg

if [[ $# -eq 0 ]]; then
	echo "Downloading latest Firefox from mozilla..."
	rm -rf $"repo"/Firefox*.dmg*
	namefile="$repo/Firefox.dmg"
	curl --location-trusted "$url" -o "$namefile" || wget --content-disposition "$url" -O "$namefile"  2>/dev/null
else
	namefile=$1
fi

echo "Mounting Firefox image"
vol=$(hdiutil attach "$namefile" -shadow | tail -n 1 | cut -f 3)

echo "Making Firefox great again"

cd "$vol" || exit 1
codesign --remove-signature "$app"

cd "$app/Contents" || exit 1
rm -rf _CodeSignature Library/LaunchServices/org.mozilla.updater
rm CodeResources

cd MacOS || exit 1
rm -rf plugin-container.app/Contents/_CodeSignature \
crashreporter.app \
updater.app \
pingsender

cd ../Resources || exit 1
rm -rf update-settings.ini updater.ini
cp -aX "$repo/settings/." .
cp "$repo/media/librewolf.icns" ./firefox.icns

cd browser/features || exit 1
rm -rf aushelper@mozilla.org.xpi \
	firefox@getpocket.com.xpi \
	onboarding@mozilla.org.xpi

cd "$vol" || exit 1
mv "$app" "LibreWolf.app"
cp "$repo/media/VolumeIcon.icns" ./.VolumeIcon.icns
cp "$repo/media/background.png" .background/background.png
rm -rf .fseventsd

cd "$repo" || exit 1

echo "Unmount Firefox image"
hdiutil detach "$vol" > /dev/null

echo "Cooking LibreWolf image"
hdiutil convert -format UDZO -o "$repo/../LibreWolf.dmg" "$namefile" -shadow > null

rm -rf "$repo"/Firefox*.dmg*

echo "All done."
echo "LibreFox img is in $repo"
